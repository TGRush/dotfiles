#!/bin/bash

set -eux

function _swww() {
	swww img "$1" --transition-type outer --transition-fps 30 --transition-duration "0.5" --transition-step 255
}

function _dark() {
	_swww /home/alexia/Pictures/walls/Wallpaper_space_orbit_H_5.png &
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark' &
	gsettings set org.gnome.desktop.interface gtk-theme adw-gtk3-dark &
}

function _light() {
	_swww /home/alexia/Pictures/walls/light-clouds.jpg &
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-light' &
	gsettings set org.gnome.desktop.interface gtk-theme adw-gtk3 &
}

echo "\$1: $1"
echo "\$@: $@"

case $1 in
	prefer-dark)
		_dark
		;;
	prefer-light)
		_light
		;;
	*)
		echo "no argument."
		;;
esac
