# This is my Dotfiles, and stuff

I vaguely remember what packages I installed, here's a rundown of the ones I remember:

```
# Void Packages:
sway
xorg
xdg-desktop-portal
xdg-desktop-portal-wlr
xdg-user-dirs
xdg-user-dirs-gtk
polkit
Waybar
wofi
dbus
NetworkManager
...
```

## Wallpaper
[Download](https://dl.uploadgram.me/627d787e7d703g)
Credits: [Pawel Czerwinski on Unsplash](https://unsplash.com/@pawel_czerwinski)
