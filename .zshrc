export PATH="$PATH:$HOME/.local/bin"
export HISTFILE="$HOME/.zsh_history"
export SAVEHIST=10000

# if [[ -v DISTROBOX_LOGIN ]]; then
#   distrobox enter fedora;
#   exit 0;
# fi

### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk

# own Config
# zinit light jackharrisonsherlock/common
zinit light zsh-users/zsh-syntax-highlighting
zinit light Moarram/headline
export "HEADLINE_LINE_MODE=off"
export "HEADLINE_STYLE_JOINT=$green"
export "HEADLINE_USER_TO_HOST= at "
zinit light joshskidmore/zsh-fzf-history-search
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Aliases

alias "ls"="eza -l"
alias "la"="eza -la"
alias "cat"="bat"
alias "vpmi"="sudo xbps-install -Sy"
alias "vpmr"="sudo xbps-remove -Ry"
alias "vpms"="sudo xbps-query -Rs"
alias "hf"="hyfetch"
alias "dbx"="distrobox"
alias "dbxe"="distrobox enter"
alias "clear"="printf '\033[2J\033[3J\033[1;1H'"

# if [[ -x "$(command -v lsb_release)" ]];then
    # return;
# else
    # alias "lsb_release"="lsb-release"
# fi
# 
# # Set PATH, MANPATH, etc., for Homebrew.
# if [[ "$(lsb_release -c | cut --delimiter=":" --fields=2 > bruh.txt && sed -i 's/    //g' bruh.txt && /usr/bin/cat bruh.txt)" == "void" ]]; then
	# eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
# else
	# return;
# fi

# Set PATH, MANPATH, etc., for Homebrew.
# (This WILL fail in Distrobox, attempted fix is commented out above.)
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

export GPG_TTY=$(tty)
# export HISTSIZE=100000

export PATH="$PATH:$HOME/.local/bin"

function sv-ssh {
	sshpass -p gigaserver ssh server@epic-server
}

function wfr {
	wf-recorder -r 30 -D -c libx264 -f "$HOME/Videos/wfr/recording-$(date -R).mkv"
}

export "MICRO_TRUECOLOR=1"

export HOMEBREW_NO_ENV_HINTS=1

source /home/linuxbrew/.linuxbrew/share/zsh/site-functions
