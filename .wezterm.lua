-- Pull in the wezterm API
local wezterm = require 'wezterm'
local config = wezterm.config_builder()
-- Divider

config = {
	color_scheme = 'catppuccin-mocha',
	font = wezterm.font 'Victor Mono',
	cell_width = 1.1,
	font_size = 10,
	use_fancy_tab_bar = false,
	tab_bar_at_bottom = true,
	hide_tab_bar_if_only_one_tab = true,
	window_padding = {
		top = 12,
		left = 12,
		right = 12,
		bottom = 12
	},
	window_background_opacity = 0.9,
	initial_cols = 132,
	initial_rows = 42,
	window_decorations = "RESIZE"
}

config.colors = {
  tab_bar = {
    background = '#0b0022',
    active_tab = {
    	bg_color = '#b4befe',
    	fg_color = '#4c4f69'
    },
    inactive_tab = {
    	bg_color = '#313244',
    	fg_color = '#a6adc8'
    },
    new_tab = {
    	bg_color = '#313244',
    	fg_color = '#a6adc8'
    }
  }
}

return config
